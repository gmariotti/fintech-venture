package challenge.mocks

import challenge.Customer
import challenge.Strategy
import challenge.configurations.CsvConfiguration
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.LocalDate

class LocalFileLoaderTest {
    private val csvConfiguration = CsvConfiguration(
        customerColumns = listOf(
           "customerId",
            "email",
            "dateOfBirth",
            "riskLevel",
            "retirementAge"
        ), strategyColumns = listOf(
            "strategyId",
            "minRiskLevel",
            "maxRiskLevel",
            "minYearsToRetirement",
            "maxYearsToRetirement",
            "stocksPercentage",
            "cashPercentage",
            "bondsPercentage"
        )
    )

    @Test
    fun `load customer example file`() {
        val classLoader = this.javaClass.classLoader
        val file = classLoader.getResource("fake-data/customers.csv")?.file

        assertThat(file).isNotNull()
        StepVerifier
            .create(csvConfiguration.localFileLoader().loadCustomers(file!!))
            .expectNext(
                Customer(
                    customerId = "1",
                    dateOfBirth = LocalDate.parse("1961-04-29"),
                    riskLevel = 3,
                    retirementAge = 65
                )
            ).expectNext(
                Customer(
                    customerId = "2",
                    dateOfBirth = LocalDate.parse("1978-05-01"),
                    riskLevel = 8,
                    retirementAge = 67
                )
            ).verifyComplete()
    }

    @Test
    fun `load strategy example file`() {
        val classLoader = this.javaClass.classLoader
        val file = classLoader.getResource("fake-data/strategy.csv")?.file

        assertThat(file).isNotNull()
        val strategies = csvConfiguration.localFileLoader().loadStrategies(file!!)
        assertThat(strategies).isEqualTo(expectedStrategies)
    }
}

private val expectedCustomers = listOf(
    Customer(
        customerId = "1",
        dateOfBirth = LocalDate.parse("1961-04-29"),
        riskLevel = 3,
        retirementAge = 65
    ),
    Customer(
        customerId = "2",
        dateOfBirth = LocalDate.parse("1978-05-01"),
        riskLevel = 8,
        retirementAge = 67
    )
)

private val expectedStrategies = listOf(
    Strategy(
        strategyId = "1",
        minRiskLevel = 0,
        maxRiskLevel = 3,
        minYearsToRetirement = 20,
        maxYearsToRetirement = 30,
        stocksPercentage = 20,
        cashPercentage = 20,
        bondsPercentage = 60
    ),
    Strategy(
        strategyId = "2",
        minRiskLevel = 0,
        maxRiskLevel = 3,
        minYearsToRetirement = 10,
        maxYearsToRetirement = 20,
        stocksPercentage = 10,
        cashPercentage = 20,
        bondsPercentage = 70
    ),
    Strategy(
        strategyId = "3",
        minRiskLevel = 6,
        maxRiskLevel = 9,
        minYearsToRetirement = 20,
        maxYearsToRetirement = 30,
        stocksPercentage = 10,
        cashPercentage = 0,
        bondsPercentage = 90
    )
)
