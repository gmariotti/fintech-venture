package challenge

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.consumer.junit5.ProviderType
import au.com.dius.pact.core.model.RequestResponsePact
import au.com.dius.pact.core.model.annotations.Pact
import au.com.dius.pact.core.model.annotations.PactFolder
import challenge.configurations.FinancialPortfolioServiceConfiguration
import io.pactfoundation.consumer.dsl.newJsonObject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.web.reactive.function.client.WebClient
import reactor.kotlin.test.verifyError
import reactor.test.StepVerifier

@ExtendWith(PactConsumerTestExt::class)
@PactTestFor("financial-portfolio-service", providerType = ProviderType.SYNCH)
@PactFolder("build/pacts/")
class FinancialPortfolioServiceTest {

    @Pact(consumer = CONSUMER)
    fun existingCustomer(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .given("customer exists for customerId 1")
            .uponReceiving("retrieve customer")
            .path("/customer/1")
            .method("GET")
            .willRespondWith()
            .status(200)
            .body(newJsonObject {
                stringType("customerId")
                // TODO - improve by making sure that the value is a positive integer
                numberType("stocks", "bonds", "cash")
            })
            .toPact()
    }

    @Test
    @PactTestFor(pactMethod = "existingCustomer")
    fun `return customer if it exist`(mockServer: MockServer) {
        val client = FinancialPortfolioServiceConfiguration(mockServer.getUrl())
            .financialPortfolioService(WebClient.builder())

        StepVerifier
            .create(client.getCustomerDetails("1"))
            .expectNextCount(1)
            .verifyComplete()
    }

    @Pact(consumer = CONSUMER)
    fun notExistingCustomer(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .given("customer does not exist")
            .uponReceiving("retrieve customer")
            .path("/customer/1")
            .method("GET")
            .willRespondWith()
            .status(404)
            .toPact()
    }

    @Test
    @PactTestFor(pactMethod = "notExistingCustomer")
    fun `throw exception if customer does not exist`(mockServer: MockServer) {
        val client = FinancialPortfolioServiceConfiguration(mockServer.getUrl())
            .financialPortfolioService(WebClient.builder())

        StepVerifier
            .create(client.getCustomerDetails("1"))
            .verifyError<CustomerNotExistent>()
    }
}

private const val CONSUMER = "fintech-venture-service"
