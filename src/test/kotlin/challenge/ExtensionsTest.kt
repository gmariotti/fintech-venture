package challenge

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.math.BigInteger

class ExtensionsTest {

    @Test
    fun `customer details are mapped to expected trade request`() {
        val expectedTradeRequest = TradeRequest(
            customerId = exampleCustomerDetails.customerId,
            stocks = (-5870).toBigInteger(),
            cash = 1260.toBigInteger(),
            bonds = 4610.toBigInteger()
        )

        assertThat(exampleCustomerDetails.toTradeRequest(exampleStrategy)).isEqualTo(expectedTradeRequest)
    }

    @Test
    fun `expected investments are rounded down`() {
        val expectedTradeRequest = TradeRequest(
            customerId = exampleCustomerDetails.customerId,
            cash = BigInteger.ZERO,
            bonds = 12.toBigInteger(),
            stocks = (-13).toBigInteger()
        )
        val strategy = exampleStrategy.copy(
            cashPercentage = 0,
            bondsPercentage = 66,
            stocksPercentage = 34
        )
        // expected bonds are 45.54 --> rounded to 45
        // expected stocks are 23.46 --> rounded to 23
        val customerDetails = exampleCustomerDetails.copy(
            cash = BigInteger.ZERO,
            bonds = 33.toBigInteger(),
            stocks = 36.toBigInteger()
        )

        assertThat(customerDetails.toTradeRequest(strategy)).isEqualTo(expectedTradeRequest)
    }
}

private val exampleCustomerDetails = CustomerDetails(
    customerId = "customerId",
    bonds = 1200.toBigInteger(),
    cash = 400.toBigInteger(),
    stocks = 6700.toBigInteger()
)

private val exampleStrategy = defaultStrategy.copy(
    stocksPercentage = 10,
    cashPercentage = 20,
    bondsPercentage = 70
)
