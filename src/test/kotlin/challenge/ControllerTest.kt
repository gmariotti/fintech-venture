package challenge

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import java.math.BigInteger
import java.time.LocalDate

@WebFluxTest(controllers = [Controller::class])
class ControllerTest {
    @Autowired lateinit var client: WebTestClient
    @MockkBean lateinit var fileLoader: FileLoader
    @MockkBean lateinit var fpService: FinancialPortfolioService

    @Test
    fun `example customers are properly processed`() {
        val request = RebalanceRequest(
            customersFile = "customersFile",
            strategyFile = "strategyFile",
            batchSize = 1
        )
        val exampleCustomer = Customer(
            customerId = "customerId",
            dateOfBirth = LocalDate.ofYearDay(1990, 1),
            retirementAge = 60,
            riskLevel = 0
        )
        val exampleCustomerDetails = CustomerDetails(
            customerId = exampleCustomer.customerId,
            stocks = BigInteger.TEN,
            bonds = BigInteger.ONE,
            cash = BigInteger.ZERO
        )
        every {
            fileLoader.loadCustomers(request.customersFile)
        } returns listOf(exampleCustomer, exampleCustomer, exampleCustomer).toFlux()
        every { fileLoader.loadStrategies(request.strategyFile) } returns listOf(defaultStrategy)
        every { fpService.getCustomerDetails(exampleCustomer.customerId) } returns exampleCustomerDetails.toMono()
        every { fpService.executeTrades(match { it.size == 1 }) } returns Unit.toMono()

        client
            .post()
            .uri("/rebalance")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(request)
            .exchange()
            .expectStatus().isNoContent
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            """{"customersFile": "", "strategyFile": "valid-file.csv", "batchSize": 1}""",
            """{"customersFile": "  ", "strategyFile": "valid-file.csv", "batchSize": 1}""",
            """{"customersFile": "valid-file.csv", "strategyFile": "", "batchSize": 1}""",
            """{"customersFile": "valid-file.csv", "strategyFile": "  ", "batchSize": 1}""",
            """{"customersFile": "valid-file.csv", "strategyFile": "valid-file.csv", "batchSize": 0}""",
            """{"customersFile": "valid-file.csv", "strategyFile": "valid-file.csv"}""",
            """{"customersFile": "valid-file.csv", "batchSize": 0}""",
            """{"strategyFile": "valid-file.csv", "batchSize": 0}"""
        ]
    )
    fun `body is validated`(invalidRequests: String) {
        client
            .post()
            .uri("/rebalance")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(invalidRequests)
            .exchange()
            .expectStatus().isBadRequest
    }
}
