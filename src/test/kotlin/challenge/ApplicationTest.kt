package challenge

import com.ninjasquad.springmockk.MockkBean
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@ActiveProfiles("local", "test")
class ApplicationTest {
    @Autowired lateinit var client: WebTestClient

    @ParameterizedTest
    @ValueSource(
        strings = ["/health/liveness", "/health/readiness", "/prometheus"]
    )
    fun `verify expected endpoints are exposed`(endpoint: String) {
        client
            .get()
            .uri(endpoint)
            .exchange()
            .expectStatus().is2xxSuccessful
    }

    @Test
    fun `example request is properly executed`() {
        val classLoader = this.javaClass.classLoader
        val request = RebalanceRequest(
            customersFile = classLoader.getResource("fake-data/customers.csv")?.file!!,
            strategyFile = classLoader.getResource("fake-data/strategy.csv")?.file!!,
            batchSize = 1
        )

        client
            .post()
            .uri("/rebalance")
            .bodyValue(request)
            .exchange()
            .expectStatus().isNoContent
    }
}
