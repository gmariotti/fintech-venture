package challenge.configurations

import challenge.FinancialPortfolioService
import org.hibernate.validator.constraints.URL
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient

@ConstructorBinding
@ConfigurationProperties("fps")
data class FinancialPortfolioServiceConfiguration(
    @field:URL val baseUrl: String
) {

    @Bean fun financialPortfolioService(builder: WebClient.Builder): FinancialPortfolioService {
        val webClient = builder.baseUrl(baseUrl).build()
        return FinancialPortfolioService(webClient)
    }
}
