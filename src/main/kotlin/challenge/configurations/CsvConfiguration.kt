package challenge.configurations

import challenge.mocks.LocalFileLoader
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import javax.validation.constraints.Size

@ConstructorBinding
@ConfigurationProperties("csv")
data class CsvConfiguration(
    @field:Size(min = 8, max = 8) val customerColumns: List<String>,
    @field:Size(min = 8, max = 8) val strategyColumns: List<String>
) {

    @Bean
    @Profile("local")
    fun localFileLoader() = LocalFileLoader(
        customerColumns = customerColumns, strategyColumns = strategyColumns
    )
}
