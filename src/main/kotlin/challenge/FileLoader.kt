package challenge

import reactor.core.publisher.Flux
import java.time.LocalDate

interface FileLoader {
    fun loadCustomers(customersFile: String): Flux<Customer>
    fun loadStrategies(strategyFile: String): List<Strategy>
}

data class Customer(
    val customerId: String,
    val dateOfBirth: LocalDate,
    val riskLevel: Int,
    val retirementAge: Int
)

data class Strategy(
    val strategyId: String,
    val minRiskLevel: Int,
    val maxRiskLevel: Int,
    val minYearsToRetirement: Int,
    val maxYearsToRetirement: Int,
    val stocksPercentage: Int,
    val cashPercentage: Int,
    val bondsPercentage: Int
)
