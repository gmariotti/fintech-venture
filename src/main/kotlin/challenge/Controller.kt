package challenge

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.LocalDate
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive

@RestController
class Controller(
    private val fileLoader: FileLoader,
    private val fpService: FinancialPortfolioService
) {

    @PostMapping("/rebalance")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun rebalance(@Valid @RequestBody request: RebalanceRequest): Mono<Unit> {
        val customers = fileLoader.loadCustomers(request.customersFile)
        val strategies = fileLoader.loadStrategies(request.strategyFile)
        val currentDate = LocalDate.now()
        return customers
            .map { it to strategies.firstMatching(it, currentDate.year) }
            .window(request.batchSize)
            .flatMap { it.processBatch() }
            .count()
            .map { Unit }
    }

    private fun Flux<CustomerWithStrategy>.processBatch(): Mono<Unit> =
        flatMap { (customer, strategy) ->
            fpService
                .getCustomerDetails(customer.customerId)
                .map { it.toTradeRequest(strategy) }
        }.collectList()
            .flatMap { fpService.executeTrades(it) }
}

data class RebalanceRequest(
    @field:NotBlank val customersFile: String,
    @field:NotBlank val strategyFile: String,
    @field:Positive val batchSize: Int
)

typealias CustomerWithStrategy = Pair<Customer, Strategy>
