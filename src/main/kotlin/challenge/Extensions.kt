package challenge

import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode

private fun Customer.yearsToRetirement(year: Int): Int {
    val age = year - dateOfBirth.year
    return this.retirementAge - age
}

private fun Customer.isValidStrategy(yearsToRetirement: Int, strategy: Strategy): Boolean {
    return riskLevel in (strategy.minRiskLevel..strategy.maxRiskLevel) &&
        yearsToRetirement in (strategy.minYearsToRetirement..strategy.maxYearsToRetirement)
}

fun List<Strategy>.firstMatching(customer: Customer, currentYear: Int): Strategy {
    val yearsToRetirement = customer.yearsToRetirement(currentYear)
    return firstOrNull { customer.isValidStrategy(yearsToRetirement, it) } ?: defaultStrategy
}

val defaultStrategy = Strategy(
    strategyId = "0",
    minRiskLevel = 0,
    maxRiskLevel = 0,
    minYearsToRetirement = 0,
    maxYearsToRetirement = 0,
    stocksPercentage = 0,
    bondsPercentage = 0,
    cashPercentage = 100
)

fun CustomerDetails.toTradeRequest(strategy: Strategy): TradeRequest {
    val totalInvestment = (stocks + cash + bonds).toBigDecimal()
    return TradeRequest(
        customerId = customerId,
        stocks = calculateInvestmentChange(totalInvestment, strategy.stocksPercentage, stocks),
        cash = calculateInvestmentChange(totalInvestment, strategy.cashPercentage, cash),
        bonds = calculateInvestmentChange(totalInvestment, strategy.bondsPercentage, bonds)
    )
}

private fun calculateInvestmentChange(
    totalInvestment: BigDecimal,
    percentage: Int,
    currentInvestment: BigInteger
): BigInteger {
    // drop decimal value only after the calculation is completed
    val expectedInvestment = totalInvestment
        .divide(hundred, maxDigits, RoundingMode.HALF_EVEN)
        .multiply(percentage.toBigDecimal())
    return expectedInvestment.toBigInteger() - currentInvestment
}

private val hundred = 100.toBigDecimal()
private val maxDigits = 2
