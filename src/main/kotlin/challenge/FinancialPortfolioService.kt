package challenge

import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import java.math.BigInteger

class FinancialPortfolioService(private val webClient: WebClient) {
    fun getCustomerDetails(customerId: String): Mono<CustomerDetails> = webClient
        .get()
        .uri("/customer/{customerId}", customerId)
        .exchange()
        .flatMap { response ->
            val statusCode = response.statusCode()
            when {
                statusCode.is2xxSuccessful -> response.bodyToMono<CustomerDetails>()
                statusCode == HttpStatus.NOT_FOUND -> Mono.error(CustomerNotExistent(customerId))
                else -> response
                    .bodyToMono<String>()
                    .defaultIfEmpty("")
                    .flatMap { Mono.error<CustomerDetails>(UnexpectedError(statusCode.value(), it)) }
            }
        }

    fun executeTrades(trades: List<TradeRequest>): Mono<Unit> = webClient
        .post()
        .uri("/execute")
        .bodyValue(trades)
        .exchange()
        .flatMap { response ->
            val statusCode = response.statusCode()
            when {
                statusCode.is2xxSuccessful -> response.releaseBody().map { Unit }
                else -> response
                    .bodyToMono<String>()
                    .defaultIfEmpty("")
                    .flatMap { Mono.error<Unit>(UnexpectedError(statusCode.value(), it)) }
            }
        }
}

data class CustomerDetails(
    val customerId: String,
    val stocks: BigInteger,
    val bonds: BigInteger,
    val cash: BigInteger
)

data class TradeRequest(
    val customerId: String,
    val stocks: BigInteger,
    val bonds: BigInteger,
    val cash: BigInteger
)

sealed class FinancialPortfolioServiceException(message: String) : Throwable(message)

class CustomerNotExistent(val customerId: String) : FinancialPortfolioServiceException(
    "customer $customerId does not exist"
)

class UnexpectedError(statusCode: Int, response: String) : FinancialPortfolioServiceException(
    "received $statusCode - $response"
)
