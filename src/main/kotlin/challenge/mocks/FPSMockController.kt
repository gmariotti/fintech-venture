package challenge.mocks

import challenge.CustomerDetails
import challenge.TradeRequest
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import kotlin.random.Random
import kotlin.random.nextUInt

@RestController
@Profile("local")
class FPSMockController {
    @GetMapping("/customer/{customerId}")
    fun getCustomer(@PathVariable customerId: String): Mono<CustomerDetails> {
        return CustomerDetails(
            customerId = customerId,
            stocks = Random.nextUInt().toLong().toBigInteger(),
            bonds = Random.nextUInt().toLong().toBigInteger(),
            cash = Random.nextUInt().toLong().toBigInteger()
        ).toMono()
    }

    @PostMapping("/execute")
    @ResponseStatus(HttpStatus.CREATED)
    fun execute(@RequestBody trades: List<TradeRequest>): Mono<Unit> {
        return Unit.toMono()
    }
}
