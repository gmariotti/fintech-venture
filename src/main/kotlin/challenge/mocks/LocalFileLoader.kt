package challenge.mocks

import challenge.Customer
import challenge.FileLoader
import challenge.Strategy
import reactor.core.publisher.Flux
import reactor.core.publisher.Flux.using
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate

class LocalFileLoader(
    private val customerColumns: List<String>,
    private val strategyColumns: List<String>
) : FileLoader {

    override fun loadCustomers(customersFile: String): Flux<Customer> =
        // Convert stream to Flux to load the file lazily
        using(
            { Files.lines(Path.of(customersFile)) },
            { Flux.fromStream(it) },
            { it.close() }
        ).skipWhile { !it.doesNotMatchesHeaders(customerColumns) }
            .map { it.split(SEPARATOR) }
            // TODO - handle invalid rows in a better way
            .filter { it.size == customerColumns.size }
            .map {
                // TODO - handle error in type conversion
                // skip email
                Customer(
                    customerId = it[0],
                    dateOfBirth = LocalDate.parse(it[2]),
                    riskLevel = it[3].toInt(),
                    retirementAge = it[4].toInt()
                )
            }

    override fun loadStrategies(strategyFile: String): List<Strategy> {
        val lines = Files.readAllLines(Path.of(strategyFile))
        val header = lines.first()
        if (header.doesNotMatchesHeaders(strategyColumns)) {
            TODO("fail if strategy file header mismatch")
        }

        return lines
            .drop(1) // drop header
            .map { it.split(SEPARATOR) }
            // TODO - handle invalid rows in a better way
            .filter { it.size == strategyColumns.size }
            .map {
                // TODO - handle error in type conversion
                Strategy(
                    strategyId = it[0],
                    minRiskLevel = it[1].toInt(),
                    maxRiskLevel = it[2].toInt(),
                    minYearsToRetirement = it[3].toInt(),
                    maxYearsToRetirement = it[4].toInt(),
                    stocksPercentage = it[5].toInt(),
                    cashPercentage = it[6].toInt(),
                    bondsPercentage = it[7].toInt()
                )
            }
    }

    private fun String.doesNotMatchesHeaders(expectedHeaders: List<String>): Boolean {
        val actualHeaders = split(SEPARATOR)
        return actualHeaders.size != expectedHeaders.size || !actualHeaders.containsAll(expectedHeaders)
    }
}

private const val SEPARATOR = ","
