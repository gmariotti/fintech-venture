# Fintech Venture Challenge

## Implementation details
I used Spring Webflux because I assume that the amount of customers that can be inside the file might be quite
high. The strategy file, instead, was just read entirely with the assumption that the number of strategies might be small
enough to be stored in memory.

Most failure cases are not covered, in particular:
- What happens when a batch fail? Should I rollback all the previous ones? For this example, one possible solution could
be to store all the customers that have a trade request send and, when the customers file is read again, all the previously
executed customers are filter out.

I used local mocks associated to a `local` spring profile to make it possible to run the application locally without the
need for external dependencies in the files and the FPS.

I wrote an example of Contract Testing for the FPS client with the idea of encouraging the definition of a contract between
the two services that would then translate into proper pipeline steps (see https://docs.pact.io/getting_started/). 

I tried to cover a minimum set of test scenarios, like the application can start with a local profile, unit tests on the
controller, a trade request is properly generated. About trade requests, I assumed that the number of assets should
always be an integer and that rounding, in this case, is an acceptable scenario. However, I would definitely reconsider
this choice based on clearer product requirements.

## Run locally

### IntelliJ

Create a `Spring Boot` configuration and set profile to `local`.

### Docker

The image can be built locally with `

    $ ./gradlew jibDockerBuild

The image can be run locally with

    $ docker run --rm -p 8080:8080 -e SPRING_PROFILES_ACTIVE=local -it fintech-venture 

## Potential Improvements:
In a real world scenario, I would use a streaming technology (SQS/Kafka/Kinesis) instead of creating an API. The reason
for this choice are the following:
- Possibility to split the customers in batches from the start and process them separately, with possibility of recovering
from errors per batch instead of per file.
- Execute the rebalancing completely asynchronously, without having to wait for the success response.

Potential downsides of this solution are mainly around feedback loop when a failure in the file structure or in processing
a batch of trades takes longer to recover from.

About the CSV files, there are few possible solutions that I would use:
- Upload them daily into an S3 bucket and having the service periodically pull the files from there. Potentially, each
file can also be streamed to make it easier to read.
- If running in Kubernetes, a possible solution would be mounting the files inside the pod and using something like
spring-cloud-kubernetes to watch for file changes.
